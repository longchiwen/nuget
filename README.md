Npm package for NuGet cli
=========
Package contains NuGet binaries. Package version is also version of NuGet. [Check out NuGet home.](https://www.nuget.org/)

## Nuget version
```
3.5.0
```

## Installation

```shell
npm install @longchiwen/nuget --save
```

## Release History

* 3.5.0 Updated nuget.exe
* 3.4.4 Updated nuget.exe
* 3.3.0 Updated nuget.exe
* 3.2.0 Initial release
